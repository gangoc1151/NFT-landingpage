import Link from "next/link"
import { styled } from '@mui/material/styles';
import { Box, Button, Card, Container, Grid, Typography } from "@mui/material";
import Image from "next/image";

interface CardNewsProps {
  img: string,
  title: string,
  description: string
}

const CardNews = ({
  img,
  title,
  description
}: CardNewsProps) => {
  return (
    <Card sx={{
      // backgroundImage: `url("${img}")`,
      height: '350px',
      position: 'relative',
      zIndex: 1
    }}>
      <Image
      src={img}
      alt=""
        width={500}
        height={400}
        style={{
          maxWidth: '100%',
          height: '200px',
          objectFit: 'cover',
          borderRadius: '4px'
        }}
      />
      <Box
        // position={'absolute'}
        // bottom={0}
        sx={{
          backgroundColor: 'white',
          display: 'flex',
          flexDirection: 'column',
          height: '140px'
          // opacity: '0.5'
        }}
        padding={'5px'}
      >
        <Typography
        sx={{
          fontWeight: '700',
          fontSize: '20px'
        }}
          component={'h5'}
        >{title}</Typography>
        <p>{description}</p>
      </Box>
    </Card>
  )
}

export const HomePageNews = () => {
  return (
    <Container
      sx={{
        paddingTop: '50px'
      }}
    >
      <Typography
        fontSize={'32px'}
        fontWeight={500}
        marginBottom={'20px'}
        color={'#084C94'}
      >
        Centrifuge News
      </Typography>
      <Grid
        container
        spacing={3}
      >
        <Grid item xs={12} sm={6} md={3}>
          <CardNews img={"/homepageNews/theYear.png"} title={"The Year of RWAs"} description={"Egestas elit dui scelerisque ut eu purus aliquam vitae habitasse."}></CardNews>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <CardNews img={"/homepageNews/launches.png"} title={"Centrifuge Announces Launches"} description={"Egestas elit dui scelerisque ut eu purus aliquam vitae habitasse."}></CardNews>

        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <CardNews img={"/homepageNews/newyork.png"} title={"New York City - December"} description={"Egestas elit dui scelerisque ut eu purus aliquam vitae habitasse."}></CardNews>

        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <CardNews img={"/homepageNews/newyear.png"} title={"New Year with tremendous momentum"} description={"Egestas elit dui scelerisque ut eu purus aliquam vitae habitasse."}></CardNews>

        </Grid>
      </Grid>
   
      
    </Container>
  )
}