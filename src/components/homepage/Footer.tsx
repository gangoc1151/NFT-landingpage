

import Link from "next/link"
import { styled } from '@mui/material/styles';
import React from 'react';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import { Box, Button, Container, InputAdornment, Typography } from "@mui/material";
import InputBase from '@mui/material/InputBase';
import { AccountCircle } from "@mui/icons-material";
import Image from "next/image";
import FacebookIcon from '@mui/icons-material/Facebook';
import XIcon from '@mui/icons-material/X';
import TelegramIcon from '@mui/icons-material/Telegram';
import YouTubeIcon from '@mui/icons-material/YouTube';

const StyledFooterWrapper = styled("div")(({theme: {breakpoints}}) => ({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
  width: '100%',
  paddingBottom: '48px',

  [breakpoints.up('md')]: {
    flexDirection: 'row'
  }
}));

const StyledSocialWrapper = styled("div")(({theme: {breakpoints}}) => ({
  display: 'flex',
  flexDirection: 'row',
  color:'white',
  marginTop: '10px',
  marginBottom: '10px'
}));

const StyledMapLinkFooterWrapper = styled("div")(({theme: {breakpoints}}) => ({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',


  [breakpoints.up('md')]: {
    flexDirection: 'row'
  }
}));


export const Footer = () => {
  return (
    <Box
     sx={{        
      color: 'white',
      backgroundColor: 'black'
    }}
    >
      <Container
        sx={{
          paddingTop: '40px',
          paddingBottom: '48px'
        }}
      >
        <StyledFooterWrapper>
          <Image
            src='storagon-logo.svg'       
            width={100}
            height={200}
              style={{
              maxWidth: '100%',
              height: 'auto',
              objectFit: 'cover'
            }}
            alt={""}      
          />
          <Box>
          </Box>
        </StyledFooterWrapper>
        <hr/>
        <StyledMapLinkFooterWrapper

        >
          <Box
            display={'flex'}
            flexDirection={'column'}
          >
            <h4>Features</h4>
            <a>Market swap</a>
            <a>About Us</a>
          </Box>
          <Box
            display={'flex'}
            flexDirection={'column'}
          >
            <h4>Resources</h4>
            <a>White paper</a>
            <a>Roadmap</a>
            <a>IDO</a>
            <a>Dfi Token</a>
          </Box>
          <Box
            display={'flex'}
            flexDirection={'column'}
          >
            <h4>Support</h4>
            <a>Help Center</a>
            <a>Contact Us</a>
          </Box>
          <Box
            display={'flex'}
            flexDirection={'column'}
          >
            <Typography
              component={'h4'}
            >Join Us</Typography>
            <StyledSocialWrapper>
              <FacebookIcon
                color='inherit'
              />
              <XIcon
                color='inherit'
                sx={{
                  marginLeft: '5px'
                }}
              />
              <TelegramIcon
                color="inherit"
                sx={{
                  marginLeft: '5px'
                }}
              />
              <YouTubeIcon
                color="inherit"
                sx={{
                  marginLeft: '5px'
                }}
              />
            </StyledSocialWrapper>
            <Box>
              <Image
                src="/footer/button2.png"
                width={130}
                height={40}
                alt=''
              />
              <Image
                style={{
                  marginLeft: '5px'
                }}
                src="/footer/button.png"
                width={130}
                height={40}
                alt=''
              />
            </Box>
          </Box>
        </StyledMapLinkFooterWrapper>
        <hr/>
        <Box
          display={'flex'}
          flexDirection={'row'}
          justifyContent={'space-between'}
        >
          <Typography>
            CompanyName @ 202X. All rights reserved.
          </Typography>
          <Typography>
            CompanyName @ 202X. All rights reserved.
          </Typography>
        </Box>
      </Container>
    </Box>

  )
}