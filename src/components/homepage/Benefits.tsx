import Link from "next/link"
import { styled } from '@mui/material/styles';
import { Box, Button, Container, Typography } from "@mui/material";
import Image from "next/image";
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';

interface BenefitCardProps {
  title: string;
  description: string;
  image: string
}

const BenefitCard = ({
  title,
  description,
  image
}: BenefitCardProps) => {
  return(
    <Card 
      variant="outlined"
      sx={{
        borderRadius: '24px',
        border: '2px solid rgba(255, 44, 104, 0.30)',
        padding: '36px 25px 23px 25px',
        height:'100%'
      }}>
      <Box
        display={'flex'}
        alignItems={'center'}
        justifyContent={'space-between'}
      >
        <Typography
          fontWeight={700}
          fontSize={'20px'}
          lineHeight={'30px'}
          component={'h4'}
          color={"#084C94"}
        >{title}</Typography>
        <Image 
          src={image} 
          alt={""}
          width={50}
          height={50}
          ></Image>
      </Box>
      <Typography
        fontWeight={400}
        fontSize={'16px'}
        lineHeight={'24px'}
        marginTop={'14px'}
      >
        {description}
      </Typography>

    </Card>
  )
}

export const Benefits = () => {
  return (
    <Box
      sx={{
        background: 'linear-gradient(#FEF3E3, white)', 
        paddingTop: '30px',
        marginBottom: '40px'
      }}
    >
    <Container>
      <Typography
        sx={{
          fontWeight: '700',
          fontSize: '28px',
          color: '#084C94'
        }}
        component={'h2'}
      >Using our tokens to trade both physical and NFT products</Typography>
      <Typography
        sx={{
          fontWeight: '400',
          fontSize: '16px',
          color: '#084C94'

        }}
        component={'h2'}>We aim to bring real-world assets onchain, we’re building a better financial system.</Typography>
      <div>
        <Grid
          sx={{
            marginTop: '30px'
          }}
        container spacing={2}>
          {benefits.map((item, index) => 
             <Grid 
              key={index}
              item xs={12} sm={6} md={4}>
                <BenefitCard title={item.title} description={item.description} image={item.image}></BenefitCard>
            </Grid>
          )}
        </Grid>
      </div>

    </Container>
    </Box>

  )
}

const benefits = [
  {
    title: 'Store benefits',
    description: 'Create your own store with us and start selling your items with all real-world matters handled by our platform. Enable your community to mint your own NFTs and order their physical representatives!',
    image:'/benefits/icon_book_database.png'
  },
  {
    title: 'Transparency',
    description: 'Obtain end-to-end transparency as the entire capital structure, securitization, and servicing of debt facilities is brought onchain - multi - tranching, NAV, privacy-first tokenization, securitization, reporting. Smart contracts can be used to automate certain aspects of the buying and selling process, ensuring that the transfer of NFTs and physical products occurs smoothly.',
    image:'/benefits/icons_transparency.png'
  },
  {
    title: 'Lower cost of capital',
    description: 'At [Your NFT Marketplace], we prioritize your financial well-being. Experience a significant reduction in transaction fees, allowing you to retain more of your investment returns. Say goodbye to unnecessary costs and hello to a more lucrative NFT trading experience.',
    image:'/benefits/icons_low_cost_capital.png'
  },
  {
    title: 'Diversification',
    description: 'Move beyond the digital realm and explore tokenized real-life products. From fashionable wearables to limited edition physical items, our marketplace transcends the boundaries of the virtual world. Immerse yourself in a vibrant collection of digital art spanning various styles, genres, and themes.',
    image:'/benefits/icons_diversification.png'
  },
  {
    title: 'Security',
    description: '[Your NFT Marketplace] now seamlessly integrates with leading DeFi protocols, enabling users to unlock liquidity from their NFT holdings. Your digital assets are no longer just static; they become dynamic instruments in the decentralized financial landscape.',
    image:'/benefits/icons_security.png'
  },
  {
    title: 'See more',
    description: 'Check our white paper',
    image:'/benefits/Icon_see_more.png'
  }
  
]