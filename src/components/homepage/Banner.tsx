import { styled } from '@mui/material/styles';
import { Box, Button, Container, Typography } from "@mui/material";
import Image from "next/image";
import LaunchIcon from '@mui/icons-material/Launch';
import Grid from '@mui/material/Grid';

const StyledButton = styled(Button)(() => ({
  background: 'linear-gradient(62deg, #2600FC 0%, #FF00EA 100%)',
  color: 'white',
  fontWeight: '700',
  padding: '5px 20px',
  borderRadius: '20px',
  marginTop: '14px',
  position: 'unset'
})); 

const StyledImageWrapper = styled("div")(({theme: {breakpoints}}) => ({
  display: 'inline',
  justifyContent: 'center'
}));



const BannerInfo = () => {
  return (
    <div>
      <Typography
        sx={{
          fontWeight: '700',
          fontSize: "30px",
          color: '#fff'
        }}
        component={'h1'}
      >
        Make your physical assets as NFTs in our platform with ease
      </Typography>
      <Typography
        sx={{
          fontSize: '17px',
          fontWeight: '400',
          lineHeight: '30px',
          color: '#fff'
        }}
      >
        We encourage creators, brands, artists to be our collaborators and operate your digital store in our NFT market
      </Typography>
      <Typography
        sx={{
          fontSize: '17px',
          fontWeight: '700',
          lineHeight: '32px',
          color: '#fff'
        }}
      >
        Are you ready?
      </Typography>
      <StyledButton>Connect wallet <LaunchIcon sx={{ marginLeft: '5px'}}/></StyledButton>
      <Box 
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: '70px'
        }}>
        <div>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '20px',
              color: '#fff'
            }}
          >Total assets Financed</Typography>
          <Typography
            sx={{
              fontWeight: '600',
              fontSize: '20px',
              color: '#fff'
            }}
          >$ 494M</Typography>
        </div>
        <div>
          <Typography sx={{
              fontWeight: '400',
              fontSize: '20px',
              color: '#fff'
            }}>Assets Tokenized</Typography>
          <Typography sx={{
              fontWeight: '600',
              fontSize: '20px',
              color: '#fff'
            }}>1368</Typography>
        </div>
        <div>
          <Typography sx={{
              fontWeight: '400',
              fontSize: '20px',
              color: '#fff'
            }}>TVL Growth (YoY)</Typography>
          <Typography sx={{
              fontWeight: '600',
              fontSize: '20px',
              color: '#fff'
            }}>+ 190%</Typography>
        </div>
      </Box>
    </div>
  )
}
export const Banner = () => {
  return (
    <Box
    sx={{
      background: "linear-gradient(62deg, #6452DF, #C34AF5, #FFE28E)",
      paddingTop: '30px'
    }}
    >
    <Container
      sx={{
        paddingTop: '40px',
        paddingBottom: '40px'
      }}
    >
      <Grid
        container 
        spacing={2}
      >
        <Grid item xs={12} md={6}>
          <BannerInfo/>
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
          
        >
          <StyledImageWrapper>
            <Image
              src="/bannerImage.png"
              width={500}
              height={400}
              style={{
                maxWidth: '100%',
                height: 'auto',
                objectFit: 'cover',

              }}
              alt={""}      
            />
          </StyledImageWrapper>
        </Grid>
      </Grid>
    </Container>
    </Box>

  )
}