import Link from "next/link"
import { styled } from '@mui/material/styles';
import { Box, Button, Container, Typography } from "@mui/material";
import Image from "next/image";
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';


const StyledContentWrapper = styled("div")(() => ({
  display: "flex",
  flexDirection: 'row',
  width: '100%'
}));


const StyledLink = styled(Link)(() => ({
  marginLeft: '58px'
}));

interface StepItemProps {
  img: string;
  title: string;
  description: string;
  isReverse: boolean;
}

const StepItem = ({
  img,
  title,
  description,
  isReverse
}: StepItemProps) => {
  return (
    <Grid 
      container 
      spacing={5}   
      direction={isReverse ? "row-reverse" : "row"}
      alignItems={'center'}
      >
      <Grid item xs={12} md={6} display={'flex'} justifyContent={'center'}>
        <Image
          src={img}        
          width={350}
          height={350}
            style={{
            maxWidth: '100%',
            height: 'auto',
            objectFit: 'cover'
          }}
          alt={""}      
        />
      </Grid>

      <Grid item xs={12} md={6}>
        <div>
          <Typography sx={{
            fontWeight: '700',
            fontSize: '24px'
            }}  
            component="h3"
            color={"#084C94"}
            >
            {title}
          </Typography>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '16px'
            }}
            component={'p'}          
          >
            {description}</Typography>
        </div>
      </Grid>
    </Grid>
  )
}

export const GuidingSteps = () => {
  return (
    <Box
    sx={{
      background: 'linear-gradient(white, #94EDFC, #94EDFC, #C4D5FF, #c392f3)',
      paddingTop: '30px'
    }}
    >
      <Container>
      <Box
        sx={{
          textAlign: 'center',
          maxWidth: '900px',
          marginLeft: 'auto',
          marginRight: 'auto'
        }}
      >
        <Typography 
          sx={{
            fontWeight: '700',
            fontSize: '30px',
            marginBottom: '10px'
          }}
          variant="h4" component="h3"
          color={'#084C94'}
          >
          How it works!
        </Typography>
        <Typography
          sx={{
            fontWeight: '400',
            fontSize: '16px',
            lineHeight: '24px'
          }}
        >
          Selling both digital and physical 
          items in an NFT marketplace involves a combination
          of digital tokenization, smart contracts, and a 
          streamlined process for handling physical fulfillment. 
          Heres a step-by-step guide on how it typically works.
        </Typography>
      </Box>

      <Box
        sx={{marginTop: '55px'}}
      >
        {guidingSteps.map((step) => (
          <StepItem
            key={step.id}
            img={step.img} 
            title={step.heading} 
            description={step.desciption} 
            isReverse={step.isReverse}        
          />
        ))}
      </Box>
      
    </Container>
    </Box>

  )
}

const guidingSteps = [
  {
    id:'1',
    img: '/GuidingStep/ListCreation.png',
    heading: 'Listing Creation',
    desciption: "Sellers create listings for their items on the NFT marketplace. Each listing includes details about the item, such as its description, price, and whether it's a digital or physical product.",
    isReverse: false
  },
  {
    id:'2',
    img: '/GuidingStep/tokenization.png',
    heading: 'Digital Item Tokenization',
    desciption: "For digital items (e.g., digital art, music, videos), sellers convert them into non-fungible tokens (NFTs). These NFTs represent ownership and authenticity and are stored on a blockchain.",
    isReverse: true
  },
  {
    id:'3',
    img: '/GuidingStep/physicalItemTokenization.png',
    heading: 'Physical Item Tokenization',
    desciption: "For physical items (e.g., merchandise, art prints), sellers associate them with NFTs. The NFT serves as a digital certificate of ownership and authenticity, while the physical item is stored or shipped separately.",
    isReverse: false
  },
  {
    id:'4',
    img: '/GuidingStep/smartContract.png',
    heading: 'Smart Contracts',
    desciption: "Smart contracts are used to automate various aspects of the transaction. For digital items, the smart contract ensures secure ownership transfer upon purchase. For physical items, the smart contract may include details about shipping, tracking, and confirmation of delivery.",
    isReverse: true
  },
  {
    id:'5',
    img: '/GuidingStep/payment.png',
    heading: 'Payment and Purchase',
    desciption: "Buyers can browse the marketplace, select the items they want (whether digital or physical), and proceed to purchase using cryptocurrency. Payment is typically made in cryptocurrency (e.g., Ethereum).",
    isReverse: false
  },
  {
    id:'6',
    img: '/GuidingStep/transferDigital.png',
    heading: 'Transfer of Digital Items',
    desciption: "Upon successful payment, the NFT representing the digital item is transferred to the buyer's digital wallet. The smart contract ensures a secure and irreversible transfer.",
    isReverse: true
  },
  {
    id:'7',
    img: '/GuidingStep/HandlingPhysical.png',
    heading: 'Handling Physical Items',
    desciption: "For physical items, the seller receives a notification of the purchase. They then proceed to ship the physical product to the buyer. The smart contract may include a confirmation mechanism, allowing the buyer to confirm receipt.",
    isReverse: false
  },
  {
    id:'8',
    img: '/GuidingStep/OwnershipTransfer.png',
    heading: 'Physical Item Ownership Transfer',
    desciption: "Sellers create listings for their items on the NFT marketplace. Each listing includes details about the item, such as its description, price, and whether it's a digital or physical product.",
    isReverse: true
  },
  {
    id:'9',
    img: '/GuidingStep/marketPlaceFee.png',
    heading: 'Marketplace Fee',
    desciption: "Sellers create listings for their items on the NFT marketplace. Each listing includes details about the item, such as its description, price, and whether it's a digital or physical product.",
    isReverse: false
  },
  {
    id:'10',
    img: '/GuidingStep/communityInteraction.png',
    heading: 'Community Interaction',
    desciption: "Sellers create listings for their items on the NFT marketplace. Each listing includes details about the item, such as its description, price, and whether it's a digital or physical product.",
    isReverse: true
  }
]