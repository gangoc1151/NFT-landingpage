

import Link from "next/link"
import { styled } from '@mui/material/styles';
import React from 'react';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import { Box, Container, Typography } from "@mui/material";

const spanStyle = {
  padding: '20px',
  background: '#efefef',
  color: '#000000'
}

const divStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundSize: 'cover',
  height: '400px',
  width: '400px'
}
const slideImages = [
  {
    url: '/GuidingStep/physicalItemTokenization.png',
    caption: 'Slide 1'
  },
  {
    url: '/GuidingStep/smartContract.png',
    caption: 'Slide 2'
  },
  {
    url: 'https://images.unsplash.com/photo-1536987333706-fc9adfb10d91?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80',
    caption: 'Slide 3'
  },
];


const StyleCard = styled('div')(() => ({
  // width: '400px'
  backgroundColor: 'white',
  padding: '26px',
  marginLeft: '10px',
  marginRight: '10px'
}));
const responsiveSettings = [
  {
      breakpoint: 650,
      settings: {
          slidesToShow: 2,
          slidesToScroll: 2
      }
  }
];

export const Feedbacks = () => {
  return (
    <Container
      sx={{
        marginTop: '75px',
        paddingBottom: '50px'
      }}
    >
      <div className="slide-container">
        <Slide slidesToScroll={1} slidesToShow={1} indicators={true} duration={5000} responsive={responsiveSettings}>
         {slideImages.map((slideImage, index)=> (
            <StyleCard key={index}>
              <Box>
                <p>Symbol</p>
              </Box>
              <Typography
               fontWeight={400}
               fontSize={'16px'}
               lineHeight={'24px'}
               color={'#111111'}
              >
                This integration not only advances DeFi but it 
                also brings many benefits to the MakerDAO ecosystem. 
                By diversifying the collateral backing DAI and adding non-crypto related assets, it increases the DAI safety and makes it extremely stable. 
                This move will also help MakerDAO meet the increasing demand of DAI by tapping in a multi trillion-dollar asset class.
              </Typography>
              <Typography 
                sx={{
                  marginTop: '24px'
                }}
              >
                Sébastien Derivaux, MakerDAO
              </Typography>
              {/* <div style={{ ...divStyle,'backgroundImage': `url(${slideImage.url})` }}>
                <span >{slideImage.caption}</span>
              </div> */}
            </StyleCard>
          ))} 
        </Slide>
      </div>
    </Container>

  )
}