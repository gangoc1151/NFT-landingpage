import Link from "next/link"
import { styled } from '@mui/material/styles';
import { Box, Button, Card, Container, Typography } from "@mui/material";
import Image from "next/image";


const StyledContentWrapper = styled("div")(() => ({
  display: "flex",
  flexDirection: 'row',
  width: '100%'
}));


const StyledLink = styled(Link)(() => ({
  marginLeft: '58px'
}));

export const LearnMore = () => {
  return (
    <Container
      sx={{
        paddingTop: '60px',

      }}
    >
      <Card sx={{
        backgroundColor: 'rgba(255, 255, 255, 0.30)',
        border: '1px solid rgba(255, 44, 104, 0.30)',
        borderRadius: '32px',
        boxShadow: 'none',
        padding: '10px',
        textAlign: 'center'
      }}>
        <Box
          maxWidth={'700px'}
          marginLeft={'auto'}
          marginRight={'auto'}
          marginBottom={'55px'}
          marginTop={'38px'}
        >
          <Typography
            sx={{
              fontSize: '30px',
              fontWeight: '700'
            }}
            component={'h2'}
            color={'#084C94'}
          >Introducing Liquidity Pools</Typography>
          <Typography
            sx={{
              fontSize: '30px',
              fontWeight: '700',
              background: 'linear-gradient(71deg, #2600FC 0%, #FF00EA 100%)',
              backgroundClip: 'text',
              WebkitTextFillColor: 'transparent'
            }}
          >Real-World Assets Everywhere</Typography>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '16px'
            }}
          >Centrifuge Liquidity Pools allow users on any supported chain - beginning with Ethereum, Arbitrum,
and Base - to invest in Centrifuge’s pools of real-world assets</Typography>
          <Button
            sx={{
              background: 'linear-gradient(51deg, #2600FC 0%, #FF00EA 100%)',
              color: 'white',
              fontWeight: '700',
              fontSize: '15px',
              padding: '7px 20px',
              borderRadius: '20px',
              marginTop: '25px',
              position: 'unset'
            }}
          >Learn more</Button>
        </Box>

      </Card>
    </Container>
  )
}