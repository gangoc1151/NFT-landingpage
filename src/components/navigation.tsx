import Link from "next/link"
import { styled } from '@mui/material/styles';
import { Box, Container, Drawer, IconButton } from "@mui/material";
import { usePathname } from "next/navigation";
import MenuIcon from '@mui/icons-material/Menu';
import { useState } from "react";

import CloseIcon from '@mui/icons-material/Close';

const StyleMainNavigation = styled("div")(({
  theme: {breakpoints}
}) => ({
  display: "flex",
  flexDirection: 'row',
  maxWidth: '1200px',
  justifyContent: 'space-between',
  paddingTop: '5px',
  paddingBottom: '5px',
  alignItems: 'center',
  marginLeft: 'auto',
  marginRight: 'auto',
  paddingLeft: '24px',
  paddingRight: '24px',
  [breakpoints.up('md')]: {
    paddingTop: '10px',
    paddingBottom: '10px',
  }
}));


const StyledLink = styled(Link)(({
  theme: {breakpoints}
}) => ({
  textDecoration: 'none',
  color: '#2B2B2B',
  borderBottom: '1px solid #000',
  padding: '10px',
  [breakpoints.up('md')]: {
    marginLeft: '58px',
    borderBottom: 'none',
    padding: 0,
    fontWeight: '400',
    fontSize: '20px',
  }


}));

const StyleIconButton = styled(IconButton)(({
  theme: {breakpoints}
}) => ({
  display: 'block',
  [breakpoints.up('md')]: {
    display: 'none'
  }
}));

const StyleDesktopMenu = styled("div")(({theme: {breakpoints}}) => ({
  display: 'none',
  [breakpoints.up('md')]: {
    display: 'block'
  }
}));

const StyleDrawer = styled(Drawer)(({theme: {breakpoints}}) => ({
  display: 'block',
  overflow: 'visible',
  [breakpoints.up('md')]: {
    display: 'none'
  },
  '& .MuiDrawer-paperAnchorRight': {
      overflow: 'visible',
      width: '80%'
  }
}));

const StyledContainer = styled('div')(({theme: {breakpoints}}) => ({
  position: 'fixed',
  backgroundColor: '#fff',
  width: '100vw',
  zIndex: 999,
  borderBottom: '1px solid #e2e8f0'
}))


export const MainNavigation = () => {

  const pathName = usePathname()
  const [state, setState] = useState(false);

  const toggleDrawer =
  (open: boolean) =>
  (event: React.KeyboardEvent | React.MouseEvent) => {
    if (
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }
  
    setState(open)
  };

  return (
    <StyledContainer>
      <StyleMainNavigation>
        <Box
          fontSize={"28px"}
          fontWeight={400}
          color={"#FFA503"}
        >
            Dfi
        </Box>
        <StyleDesktopMenu>
          {navName.map((navItem, index) => {
            const isActive = pathName.startsWith(navItem.href)
            return (
              <StyledLink 
                key={index}
                sx={{
                  color: isActive ? "#FFA502" : "#2B2B2B"
                }}
                href={navItem.href}>
                  {navItem.name}
              </StyledLink>
            )
          })}
        </StyleDesktopMenu>
        <StyleDrawer 
          anchor='right'
          open={state}
          onClose={toggleDrawer(false)}
        >
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              flexDirection: 'column',
              padding: '10px'
            }}
          >
            <h2
              style={{
                textAlign: 'center'
              }}
            >Logo</h2>
            {navName.map((navItem, index) => {
              const isActive = pathName.startsWith(navItem.href)
              return (
                <StyledLink 
                  key={index}
                  sx={{
                    color: isActive ? "#FFA502" : "#2B2B2B"
                  }}
                  href={navItem.href}>
                    {navItem.name}
                </StyledLink>
              )
          })}
          
          </Box>
          <IconButton
            sx={{
              position: 'absolute',
              top: '5px',
              color: 'white',
              left: '-35px'
            }}
            onClick={toggleDrawer(false)}
          >
            <CloseIcon fontSize="medium"/>
          </IconButton>
        </StyleDrawer>
        <StyleIconButton aria-label="delete" onClick={toggleDrawer(true)}>
          <MenuIcon></MenuIcon>
        </StyleIconButton>

      </StyleMainNavigation>
    </StyledContainer>
  )
}

const navName = [
  {
    name: "Home",
    href: '/'
  },
  {
    name: "About",
    href: '/about'
  },
  {
    name: "News",
    href: '/news'
  },
  {
    name: "Contact Us",
    href: '/contact-us'
  },
]