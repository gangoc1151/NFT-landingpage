'use client';

import { MainNavigation } from '@/components/navigation'

import { Banner } from '@/components/homepage/Banner';
import { Benefits } from '@/components/homepage/Benefits';
import { GuidingSteps } from '@/components/homepage/GuidingSteps';
import { LearnMore } from '@/components/homepage/LearnMore';
import { Feedbacks } from '@/components/homepage/Feedbacks';
import { HomePageNews } from '@/components/homepage/HomePageNews';
import { Footer } from '@/components/homepage/Footer';
import { Box } from '@mui/material';
import FacebookIcon from '@mui/icons-material/Facebook';
import XIcon from '@mui/icons-material/X';
import TelegramIcon from '@mui/icons-material/Telegram';
import YouTubeIcon from '@mui/icons-material/YouTube';

export default function Home() {
  return (
    <main style={{
      // background: 'linear-gradient(180deg, rgba(255, 213, 170, 0.20) 0%, rgba(255, 235, 215, 0.20) 11%, rgba(250, 255, 2, 0.20) 25.52%, rgba(179, 255, 83, 0.20) 50.52%, rgba(240, 64, 255, 0.20) 76.56%, rgba(255, 78, 2, 0.20) 100%);'
    }}>
      <div className="icon-bar">
        <a href="#" className="facebook">     
          <FacebookIcon color='inherit'/>
        </a> 
        <a href="#" className="twitter">              
          <XIcon color='inherit'/>
        </a> 
        <a href="#" className="google">              
          <TelegramIcon color="inherit"/>
        </a> 
        <a href="#" className="linkedin">              
          <YouTubeIcon color="inherit"/>
        </a>
      </div>

        <MainNavigation></MainNavigation>
        <Banner></Banner>
        <Benefits></Benefits>
        <GuidingSteps></GuidingSteps>
        <Box
          sx={{
            background: 'linear-gradient(#c392f3, #C4D5FF, #ceaeed)'
          }}
        >
          <LearnMore></LearnMore>
          <HomePageNews></HomePageNews>
          <Feedbacks></Feedbacks>
        </Box>

      <Footer></Footer>
    </main>
  )
}
