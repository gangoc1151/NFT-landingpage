'use client';

import { MainNavigation } from '@/components/navigation'
import styles from '../page.module.css'
import Image from "next/image";
import { Box} from "@mui/material";
import { Footer } from '@/components/homepage/Footer';

export default function ContactUs() {
  return (
    <main 
      style={{
        display: 'flex',
        flexDirection: 'column'
      }}
    >
      <MainNavigation></MainNavigation>
      <Box 
        sx={{
          backgroundImage: 'url("/contactUs/background.png")',
          width: "100%",
          height: '720px'
        }}
      >
       <div>Hello</div>
      </Box>
      <Footer></Footer>

    </main>
  )
}